﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FirmFarmForm
{
    public partial class tic : UserControl
    {
        public tic()
        {
            InitializeComponent();
        }

        public void set(bool t)
        {
            this.label1.Text = (t) ? "X" : "O";
        }
    }
}
