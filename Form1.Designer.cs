﻿namespace FirmFarmForm
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tic1 = new FirmFarmForm.tic();
            this.tic2 = new FirmFarmForm.tic();
            this.tic3 = new FirmFarmForm.tic();
            this.tic4 = new FirmFarmForm.tic();
            this.tic5 = new FirmFarmForm.tic();
            this.tic6 = new FirmFarmForm.tic();
            this.tic7 = new FirmFarmForm.tic();
            this.tic8 = new FirmFarmForm.tic();
            this.tic9 = new FirmFarmForm.tic();
            this.SuspendLayout();
            // 
            // tic1
            // 
            this.tic1.Location = new System.Drawing.Point(23, 12);
            this.tic1.Name = "tic1";
            this.tic1.Size = new System.Drawing.Size(104, 99);
            this.tic1.TabIndex = 0;
            this.tic1.Click += new System.EventHandler(this.clicat);
            // 
            // tic2
            // 
            this.tic2.Location = new System.Drawing.Point(133, 12);
            this.tic2.Name = "tic2";
            this.tic2.Size = new System.Drawing.Size(104, 99);
            this.tic2.TabIndex = 1;
            this.tic2.Click += new System.EventHandler(this.clicat);
            // 
            // tic3
            // 
            this.tic3.Location = new System.Drawing.Point(243, 12);
            this.tic3.Name = "tic3";
            this.tic3.Size = new System.Drawing.Size(104, 99);
            this.tic3.TabIndex = 2;
            this.tic3.Click += new System.EventHandler(this.clicat);
            // 
            // tic4
            // 
            this.tic4.Location = new System.Drawing.Point(243, 117);
            this.tic4.Name = "tic4";
            this.tic4.Size = new System.Drawing.Size(104, 99);
            this.tic4.TabIndex = 5;
            this.tic4.Click += new System.EventHandler(this.clicat);
            // 
            // tic5
            // 
            this.tic5.Location = new System.Drawing.Point(133, 117);
            this.tic5.Name = "tic5";
            this.tic5.Size = new System.Drawing.Size(104, 99);
            this.tic5.TabIndex = 4;
            this.tic5.Click += new System.EventHandler(this.clicat);
            // 
            // tic6
            // 
            this.tic6.Location = new System.Drawing.Point(23, 117);
            this.tic6.Name = "tic6";
            this.tic6.Size = new System.Drawing.Size(104, 99);
            this.tic6.TabIndex = 3;
            this.tic6.Click += new System.EventHandler(this.clicat);
            // 
            // tic7
            // 
            this.tic7.Location = new System.Drawing.Point(243, 222);
            this.tic7.Name = "tic7";
            this.tic7.Size = new System.Drawing.Size(104, 99);
            this.tic7.TabIndex = 8;
            this.tic7.Click += new System.EventHandler(this.clicat);
            // 
            // tic8
            // 
            this.tic8.Location = new System.Drawing.Point(133, 222);
            this.tic8.Name = "tic8";
            this.tic8.Size = new System.Drawing.Size(104, 99);
            this.tic8.TabIndex = 7;
            this.tic8.Click += new System.EventHandler(this.clicat);
            // 
            // tic9
            // 
            this.tic9.Location = new System.Drawing.Point(23, 222);
            this.tic9.Name = "tic9";
            this.tic9.Size = new System.Drawing.Size(104, 99);
            this.tic9.TabIndex = 6;
            this.tic9.Click += new System.EventHandler(this.clicat);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 402);
            this.Controls.Add(this.tic7);
            this.Controls.Add(this.tic8);
            this.Controls.Add(this.tic9);
            this.Controls.Add(this.tic4);
            this.Controls.Add(this.tic5);
            this.Controls.Add(this.tic6);
            this.Controls.Add(this.tic3);
            this.Controls.Add(this.tic2);
            this.Controls.Add(this.tic1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private tic tic1;
        private tic tic2;
        private tic tic3;
        private tic tic4;
        private tic tic5;
        private tic tic6;
        private tic tic7;
        private tic tic8;
        private tic tic9;
    }
}

