﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FirmFarmForm
{
    public partial class Form1 : Form
    {
        bool turno;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


        private void clicat(object sender, EventArgs e)
        {
            tic btn = ((tic)sender);
            btn.set(turno = !turno);
          
        }





        private static string[] SIMBOLS = new string[3] { "-", "o", "x" };
        private static bool HUMAN = true;
        private static bool ROBOT = false;
        private static int PLAYER1 = 1;
        private static int PLAYER2 = 2;

        private static int[] map = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        private static int[][] WINNERS = {
            new int[3] {1,2,3},
            new int[3] {4,5,6},
            new int[3] {7,8,9},
            new int[3] {1,4,7},
            new int[3] {2,5,8},
            new int[3] {3,6,9},
            new int[3] {1,5,9},
            new int[3] {7,5,3}
        };

        /**
         * mira si a posicio n hi ha un zero
         */
        public static bool isZero(int n)
        {
            return (map[n - 1] == 0);
        }

        /**
         * estableix posició al mapa
         * @param posicio
         * @param valor
         */
        private void setMap(int posicio, int valor)
        {
            map[posicio - 1] = valor;
        }

        private int getMap(int posicio)
        {
            return map[posicio - 1];
        }

        private void tie()
        {
            Console.WriteLine("Nobody Wins...");
        }


        private void draw()
        {
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine(this.ToString());
        }

        /**
         * retorna true si mapa ple
         * @return
         */
        public bool fullMap()
        {
            foreach (int i in map)
            {
                if (i == 0) return false;
            }
            return true;
        }

        /**
         * retorna 1/2 si algun jugador te jugada guanyadora
         * 0 si no 
         * @return
         */
        public int checkWinner()
        {
            int wins = 0;

            foreach (int[] winnerBet in WINNERS)
            {

                if (this.getMap(winnerBet[0]) == PLAYER1
                    && this.getMap(winnerBet[1]) == PLAYER1
                    && this.getMap(winnerBet[2]) == PLAYER1)
                { wins = PLAYER1; break; }

                if (this.getMap(winnerBet[0]) == PLAYER2
                    && this.getMap(winnerBet[1]) == PLAYER2
                    && this.getMap(winnerBet[2]) == PLAYER2)
                { wins = PLAYER2; break; }
            }
            return wins;
        }


        /**
         * retorna posició aleatoria triada entre les posicions zero
         * @return
         */
        private int randomPlay()
        {
            Random random = new Random();
            int rnd = 0;
            do
            {
                rnd = random.Next(0, 8) + 1;

            } while (!isZero(rnd));

            //retornem posició bona
            return rnd;
        }


        /**
         * retorna num de posicions amb valors que coincideixin amb els ints rebuts
         * @param values
         * @return
         */
        private int[] getPositionsWithValue(params int[] values)
        {
            //primer mirem mida
            int lenpos = 0;
            for (int ii = 1; ii < 10; ii++)
            {
                foreach (int i in values)
                {
                    if (getMap(ii) == i) lenpos++;
                }
            }

            int index = 0;
            int[] positions = new int[lenpos];
            for (int ii = 1; ii < 10; ii++)
            {
                foreach (int i in values)
                {
                    if (getMap(ii) == i)
                    {
                        positions[index++] = ii;
                    }
                }
            }

            return positions;
        }

        //el mateix, però retorna recompte
        private int countPositionsWithValue(params int[] values)
        {
            int[] pos = getPositionsWithValue(values);

            return pos.Length;

        }

        /**
         * Cerca apostes guanyadores entre les posicions rebudes
         * positions és un List<int> de la forma [2,3,5,6,8,9]
         * on hi ha 0s i (1s o 2s) depen de jugador
         * i zerosLeft és el nombre de posicions "0" que acceptem per donar
         * la winnerbet com a vàlida
         * si acceptem només 1 seria jugada guanyadora
         * 
         * si no es troba jugada guanyadora es retorna 0
         * si es troba, es retornarà la posició que sempre anirà de 1 a 9
         * i que és la primera de les posicions 0 lliures (z.get(0))
         * 
         * possibles millores:
         *   no retornar z.get(0) sino la jugada estratègicament més interessant (en cas que hi hagi varios 0s)
         *   
         * @param positions
         * @param positionsLeft
         * @return
         */
        private int getWinnerBet(int[] positions, int zerosLeft)
        {
            int[] z = null;
            // per cada winnerbet mirem si està continguda en el array de 
            // posicions del jugador + posicions lliures
            //...
            foreach (int[] winnerBet in WINNERS)
            {

                if (Array.IndexOf(positions, winnerBet[0]) != -1
                    && Array.IndexOf(positions, winnerBet[1]) != -1
                    && Array.IndexOf(positions, winnerBet[2]) != -1)
                {
                    z = getZeros(winnerBet);
                    if (z.Length == zerosLeft)
                    {
                        return z[0];
                    }
                }
                z = null;
            }
            return 0;
        }


        /**
         * ia reforçada!
         * the robot és el jugador IA
         * @return
         */
        private int iaPlay(TicTacToePlayer theRobot)
        {

            int[] robotPositions;
            int[] alterPositions;

            int resp;
            int iaPlayer = theRobot.getId();
            int alterPlayer = (iaPlayer == 1) ? 2 : 1;

            //en primer lloc mirem si es la nostra primera jugada
            //en cas que no ho sigui, tornem random, però podriem aplicar estratègia!
            if (countPositionsWithValue(iaPlayer) == 0)
            {
                return randomPlay(); // si és ocupat, random
            }

            //mirem si NOSALTRES podem guanyar en 1!

            robotPositions = getPositionsWithValue(0, iaPlayer);
            resp = getWinnerBet(robotPositions, 1);
            if (resp > 0) return resp; //getwinnerbet retorna 0 si no troba posició guanyadora

            //mirem si el contrari pot guanyar en 1!
            //si és axí, li robem la jugada per evitar-ho
            alterPositions = getPositionsWithValue(0, alterPlayer);
            resp = getWinnerBet(alterPositions, 1);
            if (resp > 0) return resp;


            //si hem arribat fins aquí, vol dir que ningú pot guanyar en 1... 
            for (int pass = 2; pass < 4; pass++)
            {
                if (getWinnerBet(robotPositions, pass) > 0)
                {
                    return getWinnerBet(robotPositions, pass);
                }
            }

            Console.WriteLine("rnd");
            return randomPlay();
        }



        /**
         * retorna un int[] amb les posicions zero 
         * del array de posicions rebut
         */
        private int[] getZeros(int[] arr)
        {
            int numzeros = 0;

            foreach (int i in arr)
            {
                if (getMap(i) == 0) numzeros++;
            }

            int[] zeros = new int[numzeros];
            int index = 0;
            foreach (int i in arr)
            {
                if (getMap(i) == 0) zeros[index++] = i;
            }
            return zeros;
        }

        public void log(string str)
        {
            Console.WriteLine(str);
        }

        public void play()
        {

            TicTacToePlayer player2 = new TicTacToePlayer("Simple human", PLAYER2, HUMAN);
            TicTacToePlayer player1 = new TicTacToePlayer("IA Robot", PLAYER1, ROBOT);
            TicTacToePlayer currentPlayer;

            int position;
            currentPlayer = player1;

            do
            {
                // mirem si mapa és ple o tenim un guanyador, en aquest cas sortim
                if (fullMap() || checkWinner() != 0)
                {
                    draw();
                    break;
                }

                if (currentPlayer.isRobot())
                {
                    position = iaPlay(currentPlayer);
                }
                else
                {
                    draw();
                    position = currentPlayer.play();
                }

                setMap(position, currentPlayer.getId());

                //conmuta currentPlayer entre player1 y player2
                currentPlayer = (currentPlayer == player1) ? player2 : player1;

            } while (true);


            switch (checkWinner())
            {
                case 1:
                    player1.wins();
                    break;
                case 2:
                    player2.wins();
                    break;
                default:
                    tie();
                    break;
            }

            Console.ReadKey();
        }



    }
    }
